echo CI_REGISTRY=$CI_REGISTRY >> ./.envs/.production/.django
echo CI_PROJECT_NAMESPACE=$CI_PROJECT_NAMESPACE >> ./.envs/.production/.django
echo CI_PROJECT_NAME=$CI_PROJECT_NAME >> ./.envs/.production/.django
echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> ./.envs/.production/.django

echo REDIS_IMAGE=$IMAGE:redis-prod >> ./.envs/.production/.django
echo DOCS_IMAGE=$IMAGE:docs-prod >> ./.envs/.production/.django
echo DJANGO_IMAGE=$IMAGE:django-prod >> ./.envs/.production/.django
echo POSTGRES_IMAGE=$IMAGE:postgres-prod >> ./.envs/.production/.django
echo FLOWER_IMAGE=$IMAGE:flower-prod >> ./.envs/.production/.django
echo CELERYWORKER_IMAGE=$IMAGE:celeryworker-prod >> ./.envs/.production/.django
echo CELERYBEAT_IMAGE=$IMAGE:celerybeat-prod >> ./.envs/.production/.django
echo TRAEFIK_IMAGE=$IMAGE:traefik-prod >> ./.envs/.production/.django
echo POSTGRES_IMAGE=$IMAGE:postgres-prod >> ./.envs/.production/.postgres
