ssh -o StrictHostKeyChecking=no $CONNECT_DEPLOY_INSTANCE << 'ENDSSH'
  cd sumedha
  export $(cat ./.envs/.production/.django | xargs)
  docker login -u $CI_DOCKER_DEPLOY_USER -p $CI_DOCKER_DEPLOY_KEY $CI_REGISTRY
  docker pull $IMAGE:docs-prod || true
  docker pull $IMAGE:django-prod
  docker pull $IMAGE:postgres-prod
  docker pull $IMAGE:flower-prod || true
  docker pull $IMAGE:celeryworker-prod || true
  docker pull $IMAGE:celerybeat-prod || true
  docker pull $IMAGE:traefik-prod
  docker-compose -f gitlab-production.yml up -d
  . setup.sh
ENDSSH
