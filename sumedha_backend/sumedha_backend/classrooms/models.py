from django.db import models
from django.contrib.auth import get_user_model

from sumedha_backend.subjects.models import Subject

User = get_user_model()

class ClassRoom(models.Model):
    name = models.CharField(max_length=30)
    subjects = models.ManyToManyField(Subject)

    def __str__(self):
        return self.name

class Teacher(models.Model):
    user = models.OneToOneField(User, verbose_name="User", on_delete=models.CASCADE)
    subjects = models.ManyToManyField(Subject)
    classroom = models.ManyToManyField(ClassRoom)

    def __str__(self):
        return self.user.name + self.user.username

class Student(models.Model):
    user = models.OneToOneField(User, verbose_name="User", on_delete=models.CASCADE)
    classroom = models.ForeignKey(ClassRoom, verbose_name="ClassRoom", on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.user.name + self.user.username
