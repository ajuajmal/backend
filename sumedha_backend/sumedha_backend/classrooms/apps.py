from django.apps import AppConfig


class ClassroomConfig(AppConfig):
    name = 'sumedha_backend.classrooms'
