from django.contrib import admin

from .models import ClassRoom,Teacher,Student

# Register your models here.
admin.site.register(ClassRoom)
admin.site.register(Teacher)
admin.site.register(Student)
