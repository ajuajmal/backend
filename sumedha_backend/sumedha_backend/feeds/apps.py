from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

class FeedsConfig(AppConfig):
    name = "sumedha_backend.feeds"
    verbose_name = _("Feeds")

    def ready(self):
        try:
            import sumedha_backend.feeds.signals  # noqa F401
        except ImportError:
            pass
