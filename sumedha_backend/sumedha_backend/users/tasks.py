from django.contrib.auth import get_user_model
from celery.task.schedules import crontab
from celery.decorators import periodic_task
from config import celery_app

User = get_user_model()


@celery_app.task()
def get_users_count():
    """A pointless Celery task to demonstrate usage."""
    return User.objects.count()

@periodic_task(run_every=(crontab(minute='*/15')), name="add", ignore_result=True)
def add():
    print('here hey')
    return 'hey'
