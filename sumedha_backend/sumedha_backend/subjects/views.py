from django.shortcuts import render

# Create your views here.
from .models import Material

class MaterialDetailView(LoginRequiredMixin, DetailView):

    model = Material
    slug_field = "id"
    slug_url_kwarg = "id"


material_detail_view = MaterialDetailView.as_view()
