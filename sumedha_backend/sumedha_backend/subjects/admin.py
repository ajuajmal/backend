from django.contrib import admin

from .models import Subject,Material,Category,MaterialCategory
from .models import SubjectIcon,SubjectColor,MaterialRead
# Register your models here.
admin.site.register(Subject)
admin.site.register(Material)
admin.site.register(Category)
admin.site.register(MaterialCategory)
admin.site.register(SubjectIcon)
admin.site.register(SubjectColor)
admin.site.register(MaterialRead)
