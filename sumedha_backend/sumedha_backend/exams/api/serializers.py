from sumedha_backend.exams.models import Exam, ExamTaker, Question, Answer, StudentsAnswer
from rest_framework import serializers


class ExamListSerializer(serializers.ModelSerializer):
	questions_count = serializers.SerializerMethodField()
	class Meta:
		model = Exam
		fields = ["id", "name", "description", "file", "slug", "questions_count"]
		read_only_fields = ["questions_count"]

	def get_questions_count(self, obj):
		return obj.question_set.all().count()


class AnswerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Answer
		fields = ["id", "question", "label"]


class QuestionSerializer(serializers.ModelSerializer):
	answer_set = AnswerSerializer(many=True)

	class Meta:
		model = Question
		fields = "__all__"


class StudentsAnswerSerializer(serializers.ModelSerializer):
	class Meta:
		model = StudentsAnswer
		fields = "__all__"


class StudentExamListSerializer(serializers.ModelSerializer):
	completed = serializers.SerializerMethodField()
	progress = serializers.SerializerMethodField()
	questions_count = serializers.SerializerMethodField()
	score = serializers.SerializerMethodField()
	subject = serializers.SerializerMethodField()

	class Meta:
		model = Exam
		fields = ["id", "name", "description", "file", "slug", "questions_count", "completed", "score", "progress","subject"]
		read_only_fields = ["questions_count", "completed", "progress"]

	def get_subject(self,ibj):
		return obj.subject.name

	def get_completed(self, obj):
		try:
			examtaker = ExamTaker.objects.get(user=self.context['request'].user, exam=obj)
			return examtaker.completed
		except ExamTaker.DoesNotExist:
			return None

	def get_progress(self, obj):
		try:
			examtaker = ExamTaker.objects.get(user=self.context['request'].user, exam=obj)
			if examtaker.completed == False:
				questions_answered = StudentsAnswer.objects.filter(exam_taker=examtaker, answer__isnull=False).count()
				total_questions = obj.question_set.all().count()
				return int(questions_answered / total_questions)
			return None
		except ExamTaker.DoesNotExist:
			return None

	def get_questions_count(self, obj):
		return obj.question_set.all().count()

	def get_score(self, obj):
		try:
			examtaker = ExamTaker.objects.get(user=self.context['request'].user, exam=obj)
			if examtaker.completed == True:
				return examtaker.score
			return None
		except ExamTaker.DoesNotExist:
			return None


class ExamTakerSerializer(serializers.ModelSerializer):
	studentsanswer_set = StudentsAnswerSerializer(many=True)

	class Meta:
		model = ExamTaker
		fields = "__all__"


class ExamDetailSerializer(serializers.ModelSerializer):
	examtakers_set = serializers.SerializerMethodField()
	question_set = QuestionSerializer(many=True)

	class Meta:
		model = Exam
		fields = "__all__"

	def get_examtakers_set(self, obj):
		try:
			exam_taker = ExamTaker.objects.get(user=self.context['request'].user, exam=obj)
			serializer = ExamTakerSerializer(exam_taker)
			return serializer.data
		except ExamTaker.DoesNotExist:
			return None


class ExamResultSerializer(serializers.ModelSerializer):
	examtaker_set = serializers.SerializerMethodField()
	question_set = QuestionSerializer(many=True)

	class Meta:
		model = Exam
		fields = "__all__"

	def get_examtaker_set(self, obj):
		try:
			examtaker = ExamTaker.objects.get(user=self.context['request'].user, exam=obj)
			serializer = ExamTakerSerializer(examtaker)
			return serializer.data

		except ExamTaker.DoesNotExist:
			return None
