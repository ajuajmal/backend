from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework import generics, permissions, status
from rest_framework.response import Response
from sumedha_backend.exams.models import Answer, Question, Exam, ExamTaker, StudentsAnswer
from .serializers import StudentExamListSerializer, ExamDetailSerializer, ExamListSerializer, ExamResultSerializer, StudentsAnswerSerializer




class StudentExamListAPI(generics.ListAPIView):
	permission_classes = [
		permissions.IsAuthenticated
	]
	serializer_class = StudentExamListSerializer

	def get_queryset(self, *args, **kwargs):
		queryset = Exam.objects.filter(examtaker__user=self.request.user)
		query = self.request.GET.get("q")

		if query:
			queryset = queryset.filter(
				Q(name__icontains=query) |
				Q(description__icontains=query)
			).distinct()

		return queryset


class ExamListAPI(generics.ListAPIView):
	serializer_class = ExamListSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]

	def get_queryset(self, *args, **kwargs):
		queryset = Exam.objects.filter(roll_out=True).exclude(examtaker__user=self.request.user)
		query = self.request.GET.get("q")

		if query:
			queryset = queryset.filter(
				Q(name__icontains=query) |
				Q(description__icontains=query)
			).distinct()

		return queryset


class ExamDetailAPI(generics.RetrieveAPIView):
	serializer_class = ExamDetailSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]

	def get(self, *args, **kwargs):
		slug = self.kwargs["slug"]
		exam = get_object_or_404(Exam, slug=slug)
		last_question = None
		obj, created = ExamTaker.objects.get_or_create(user=self.request.user, exam=exam)
		if created:
			for question in Question.objects.filter(exam=exam):
				StudentsAnswer.objects.create(exam_taker=obj, question=question)
		else:
			last_question = StudentsAnswer.objects.filter(exam_taker=obj, answer__isnull=False)
			if last_question.count() > 0:
				last_question = last_question.last().question.id
			else:
				last_question = None

		return Response({'exam': self.get_serializer(exam, context={'request': self.request}).data, 'last_question_id': last_question})


class SaveStudentsAnswer(generics.UpdateAPIView):
	serializer_class = StudentsAnswerSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]

	def patch(self, request, *args, **kwargs):
		examtaker_id = request.data['examtaker']
		question_id = request.data['question']
		answer_id = request.data['answer']

		examtaker = get_object_or_404(ExamTaker, id=examtaker_id)
		question = get_object_or_404(Question, id=question_id)
		answer = get_object_or_404(Answer, id=answer_id)

		if examtaker.completed:
			return Response({
				"message": "This exam is already complete. you can't answer any more questions"},
				status=status.HTTP_412_PRECONDITION_FAILED
			)

		obj = get_object_or_404(StudentsAnswer, exam_taker=examtaker, question=question)
		obj.answer = answer
		obj.save()

		return Response(self.get_serializer(obj).data)


class SubmitExamAPI(generics.GenericAPIView):
	serializer_class = ExamResultSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]

	def post(self, request, *args, **kwargs):
		examtaker_id = request.data['examtaker']
		question_id = request.data['question']
		answer_id = request.data['answer']

		examtaker = get_object_or_404(ExamTaker, id=examtaker_id)
		question = get_object_or_404(Question, id=question_id)

		exam = Exam.objects.get(slug=self.kwargs['slug'])

		if examtaker.completed:
			return Response({
				"message": "This exam is already complete. You can't submit again"},
				status=status.HTTP_412_PRECONDITION_FAILED
			)

		if answer_id is not None:
			answer = get_object_or_404(Answer, id=answer_id)
			obj = get_object_or_404(StudentsAnswer, exam_taker=examtaker, question=question)
			obj.answer = answer
			obj.save()

		examtaker.completed = True
		correct_answers = 0

		for students_answer in StudentsAnswer.objects.filter(exam_taker=examtaker):
			answer = Answer.objects.get(question=students_answer.question, is_correct=True)
			print(answer)
			print(students_answer.answer)
			if students_answer.answer == answer:
				correct_answers += 1

		examtaker.score = int(correct_answers / examtaker.exam.question_set.count() * 100)
		print(examtaker.score)
		examtaker.save()

		return Response(self.get_serializer(exam).data)
