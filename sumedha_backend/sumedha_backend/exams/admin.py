from django.contrib import admin
import nested_admin
from .models import Exam, Question, Answer, ExamTaker, StudentsAnswer


class AnswerInline(nested_admin.NestedTabularInline):
	model = Answer
	extra = 4
	max_num = 4


class QuestionInline(nested_admin.NestedTabularInline):
	model = Question
	inlines = [AnswerInline,]
	extra = 5


class ExamAdmin(nested_admin.NestedModelAdmin):
	inlines = [QuestionInline,]


class StudentsAnswerInline(admin.TabularInline):
	model = StudentsAnswer


class ExamTakerAdmin(admin.ModelAdmin):
	inlines = [StudentsAnswerInline,]


admin.site.register(Exam, ExamAdmin)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(ExamTaker, ExamTakerAdmin)
admin.site.register(StudentsAnswer)
