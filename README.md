
# Setup Dev Env

- Install Python(>=3.6) & PIP(latest)
- Create Virtual env

  `virtualenv -p python3 env`

- activate virtualenv

  `. env/bin/activate`

- Install postgresql-11

      sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

      wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

      sudo apt-get install postgresql-11

- Install libpq-dev Package

  `sudo apt-get install libpq-dev`

- Install requirements

  `pip install -r sumdeha/requirements/local.txt`

- Set postgresql

  `sudo psql -U postgres`

  - If `FATAL: Peer authentication failed for user "postgres"FATAL: Peer authentication failed for user "postgres"
` this error show then

    `sudo -u postgres psql`

  - change password
    `\password postgres`

  - quit from psql

    `\q`



  - Edit `/etc/postgresql/<postgres-version>/main/pg_hba.conf` and change:

      eg:- `sudo nano /etc/postgresql/11/main/pg_hba.conf`

  change below   

          local    all        postgres                               peer

    To
          local    all        postgres                               md5

  - Restart postgresql::

  `sudo service postgresql restart`


  - Login to the postgres shell and run:

        CREATE DATABASE sumedha;
        GRANT ALL PRIVILEGES ON DATABASE sumedha TO postgres;
        \q

- Install [Docker engine](https://docs.docker.com/install/)
- Install [Docker-compose](https://docs.docker.com/compose/install/)
- Install [Docker-Machine](https://docs.docker.com/machine/install-machine/)

  - Export database Vars to enviornment

        `export DATABASE_URL=postgres://postgres:<password>@127.0.0.1:5432/sumedha`

  - Run a docker-machine

    `docker-machine create --driver virtualbox dev`

    `eval $(docker-machine env dev)`



  - Build docker images

	`sudo docker-compose -f local.yml build`

  - Run docker images

	`sudo docker-compose -f local.yml up -d`

  - FIRE it http://0.0.0.0:8000/v1/ping/

  - If error happens then check for docker container logs

    `sudo docker-compose -f local.yml logs`
